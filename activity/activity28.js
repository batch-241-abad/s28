// Insert a single room (insertOne method) 

db.hotel.insertOne({
	name: "single",
	accomodates: "2",
	price: "1000",
	description: "A simple room with all the basic necessities",
	room_available: "10",
	isAvailable: "false"
});





// Insert multiple rooms (insertMany method)
db.hotel.insertMany([{
	name: "double",
	accomodates: "3",
	price: "2000",
	description: "A simple room fir for a small family going on a vacation",
	room_available: "5",
	isAvailable: "false"
	},
	{
	name: "queen",
	accomodates: "4",
	price: "4000",
	description: "A room with a queen sized bed perfect for a simple getaway",
	room_available: "15",
	isAvailable: "false"
	}]);


// Use the find method to search for a room with the name double.

db.hotel.find({name: "double"})


// Use the updateOne method to update the queen room and set the available rooms to 0.

db.hotel.updateOne(
	{name: "queen"},
	{
		$set: {
			room_available: "0"
				}
	}
	
);
db.hotel.find({name: "queen"})


// Use the deleteMany method to delete all rooms that have 0 rooms available.

db.hotel.deleteMany({room_available: "0"});
db.hotel.find();