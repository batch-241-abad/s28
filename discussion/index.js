/*

CRUD OPERATIONS


create read ukpdate and delete

-crud is the heart of any backend application
-mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods

*/

// CREATE: inserting documents

/*

	INSERTING ONE: 

	SYNTAX:
		db.collectionName.insertOne({})

	inserting or assigning values in JS objects:
		object.object.method({})

*/
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});


/*
	INSERT MANY
	Syntax:
		db.users.insertMany([
		{objectA}, {objectB} 
		]);
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5878787",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "NONE"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 76,
		contact: {
			phone: "55555",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}
	]);


/*
	READ: FINDING DOCUMENTS

	FIND ALL

	Syntax:
		db.collectionName.find();
*/
db.users.find();

/*
	FINDING USERS WITH SINGLE PARAMETERS

	SYNTAX:
		db.collectionName.find({field: value})

	

	LOOK FOR STEPHEN HAWKING USING FIRSTNAME:
*/
db.users.find({firstName: "Stephen"})



// FINDING USERS WITH MULTIPLE ARGUMENTS

// no matches
db.users.find({firstName: "Stephen", age: 20});

// WITH matching one
db.users.find({firstName: "Stephen", age: 76});



// UPDATE: updating documents

// repeat Jane to be updated
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

// UDPATE ONE

/*
	SYNTAX:
		db.collectionName.updateOne({criteria}, {$set: (field: value)})
*/

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "000000",
				email: "janegates@gmail.com"
			},
			courses: ["AWS", "Google Clouud", "Azure"],
			department: "none",
			status: "active"
				}
	}
	
);
db.users.find({firstName: "Jane"});


// UPDATE MANY

db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
			}
	}
	
);
db.users.find().pretty();


// REPLACE ONE
/*
	CAN BE USED if replacing the whole doc is necessary

	SYNTAX:
		db.collectionName.replaceOne({criteria}, {$set: {field: value}})
*/

db.users.replaceOne(
	{lastName: "Gates"}, 
	{		
			firstName: "Bill",
			lastName: "Clinton"
		
	})
db.users.find({firstName: "Bill"});

// DELETE: DELETING DOCUMENTS
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})
db.users.find({firstName: "Test"})

// deleting a single document:

/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/
db.users.deleteOne({firstName: "Test"})
db.users.find({firstName: "Test"})


// delete many

db.users.deleteMany({
	courses: []
});
db.users.find();


/*
	DELETE ALL
	db.users.delete()
	-when using the "deleteMany" method, if you search criteria is provided, it iwll delete all documents
	-DO NOT USE: db.collectionName.deleteMany()

	SYNTAX:
		db.collectionName.deletemany({criteria})
*/



/*
	ADVANCED QUERIES

	Query an embedded document
*/

db.users.find({
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	}
});

// find the document with the email "janedoe@gmail.com"
// queryin on nested fields:
db.users.find({"contact.email": "janedoe@gmail.com"
});


db.users.find({courses: [ 
        "CSS", 
        "JavaScript", 
        "Python"
    ]})


// querying an element without regard to order
db.users.find({courses: {$all: [ "JavaScript", "CSS", "Python"]}})


// make an array to  query
db.users.insert({
	namearr: [{
		namea: "juan"
	},{
		nameb: "tamad"
	}]
})
db.users.find(namea: "juan")


// find

db.users.find({
	namearr: {
		namea: "juan"
	}
})